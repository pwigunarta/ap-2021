package id.ac.ui.cs.tutorial0.controller;

import id.ac.ui.cs.tutorial0.service.AdventurerCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        int power = adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear);
        String powerClass = "";
        if(power <= 20000 && power > 0){
            powerClass = "C class";
        }else if (power <= 10000 && power > 20000){
            powerClass = "B class";
        }else if (power > 100000){
            powerClass = "A class";
        }
        model.addAttribute("power", power );
        model.addAttribute("powerClass", powerClass);
        return "calculator";
    }
}
